module.exports = (function () {

    var mongoose = require('mongoose')
        , Schema = mongoose.Schema
        , _ = require('underscore')
        , findOrCreate = require('mongoose-findorcreate')
        , mongooseLong = require('mongoose-long')(mongoose)
        , SchemaTypes = mongoose.Schema.Types
        , autoIncrement = require('mongoose-auto-increment')
        ;
    var connection = mongoose.createConnection("mongodb://localhost/managementsystem");

    autoIncrement.initialize(connection);

    var activitiesSchema = new Schema({}, {strict: false});

    activitiesSchema.plugin(findOrCreate);

    return mongoose.model('activities', activitiesSchema,'activities');

}());