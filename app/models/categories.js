module.exports = (function () {

    var mongoose = require('mongoose')
        , Schema = mongoose.Schema
        , _ = require('underscore')
        , findOrCreate = require('mongoose-findorcreate')
        , mongooseLong = require('mongoose-long')(mongoose)
        , SchemaTypes = mongoose.Schema.Types
        , autoIncrement = require('mongoose-auto-increment')
        ;
    var connection = mongoose.createConnection("mongodb://localhost/managementsystem");

    autoIncrement.initialize(connection);

    var categoriesSchema = new Schema ({

        categorie : {
            type : String,
            require :  true
        },
        
        status : {
            type : String,
            default :  10
        },
        date : {
            type : String,
            default : new Date()
        }

    });

    return mongoose.model('categories', categoriesSchema);

}());