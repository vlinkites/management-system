module.exports = (function () {

    var mongoose = require('mongoose')
        , Schema = mongoose.Schema
        , _ = require('underscore')
        , findOrCreate = require('mongoose-findorcreate')
        , mongooseLong = require('mongoose-long')(mongoose)
        , SchemaTypes = mongoose.Schema.Types
        , autoIncrement = require('mongoose-auto-increment')
        ;
    var connection = mongoose.createConnection("mongodb://localhost/managementsystem");

    autoIncrement.initialize(connection);

    var documentsSchema = new Schema({}, {strict: false});

    documentsSchema.plugin(findOrCreate);

    return mongoose.model('documents', documentsSchema,'documents');

}());