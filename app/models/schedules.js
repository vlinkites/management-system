module.exports = (function () {

    var mongoose = require('mongoose')
        , Schema = mongoose.Schema
        , _ = require('underscore')
        , findOrCreate = require('mongoose-findorcreate')
        , mongooseLong = require('mongoose-long')(mongoose)
        , SchemaTypes = mongoose.Schema.Types
        , autoIncrement = require('mongoose-auto-increment')
        ;
    var connection = mongoose.createConnection("mongodb://localhost/managementsystem");

    autoIncrement.initialize(connection);

    var schedulesSchema = new Schema({
        is_deleted:{
            type : Boolean,
            default : false
        }
    }, {strict: false});

    schedulesSchema.plugin(findOrCreate);
    return mongoose.model('schedules', schedulesSchema,'schedules');
}());