var app = angular.module('management_system',['ngRoute']);
app.config(function($routeProvider) {
	$routeProvider.when('/',{
	templateUrl  : 'pages/sigin.html',
	controller  : 'mainController'
	})
	.when('/forgot', {
		templateUrl : 'pages/forgot_password.html',
		controller  : 'mainController'
	})
	.when('/signup', {
		templateUrl : 'pages/signup.html',
		controller  : 'mainController'
	})
	
});
