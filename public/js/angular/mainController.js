app.controller('mainController',function($scope,$http, $window) {
	$scope.title = "Management System";
	$scope.login = function()
	{
      $http.post('/login', {email : $scope.email, password : $scope.password})
		.success(function(data){
			if(data.message == 2){
                $window.location.href = "/home";
				} else {
				$scope.loginError = data;
			}
		})
		.error(function(data){
			console.log(data);
		});
	}
});